#!/bin/bash
# Prepare database server

# ==================== [ Variables ] ====================

MYSQL_REPO_LINK=https://dev.mysql.com/get/mysql-apt-config_0.8.24-1_all.deb

DB_NAME=wordpress
DB_USER=wordpress
DB_PASSWORD=wordpress
DB_HOST=192.168.56.11

WP_IP_ADDRESS=192.168.56.12

# ==================== [ General ] ====================

apt update && apt install -y sudo vim mc tree gnupg2 wget curl ca-certificates lsb-release debian-archive-keyring

# ==================== [ Main ] ====================

# --- [ Install MySQL] ---

# Add MySQL repository
export DEBIAN_FRONTEND=noninteractive   # For non-interactive installation mysql-repo.deb and mysql-server
sudo wget --output-document=mysql-repo.deb $MYSQL_REPO_LINK
sudo -E apt install ./mysql-repo.deb

# Add key without using deprecated apt-key
#sudo gpg --no-default-keyring --keyring gnupg-ring:/etc/apt/trusted.gpg.d/mysql.gpg --keyserver pgp.mit.edu --recv 3A79BD29
#sudo chmod 644 /etc/apt/trusted.gpg.d/mysql.gpg
#sudo echo "deb [signed-by=/etc/apt/trusted.gpg.d/mysql.gpg] http://repo.mysql.com/apt/debian $(lsb_release -cs) mysql-8.0" > /etc/apt/sources.list.d/mysql.list

# Install mysql-server
sudo apt update && sudo -E apt install -y mysql-server

# Start MySQL and enable automatic start
sudo systemctl enable mysql --now

# --- [ Prepare MySQL for usage with wordpress ] ---

sudo mysql -e "CREATE DATABASE $DB_NAME;"
sudo mysql -e "CREATE USER '$DB_USER'@'$WP_IP_ADDRESS' IDENTIFIED BY '$DB_PASSWORD';"
sudo mysql -e "GRANT ALL PRIVILEGES ON $DB_NAME.* TO '$DB_USER'@'$WP_IP_ADDRESS';"
sudo mysql -e "FLUSH PRIVILEGES;"