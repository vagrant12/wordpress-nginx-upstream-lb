#!/bin/bash
# Prepare load balancer

# ==================== [ Variables ] ====================

NGINX_DEFAULT_CONF=/etc/nginx/conf.d/default.conf
NGINX_SITES_LOCATION=/usr/share/nginx
NGINX_ACCESS_LOG_DIR=/var/log/nginx
NGINX_SERVER_NAME=lb

NGINX_UPSTREAM_SERVER1=192.168.56.12
NGINX_UPSTREAM_SERVER2=192.168.56.13
NGINX_UPSTREAM_SERVER3=192.168.56.14

# ==================== [ General ] ====================

apt update && apt install -y sudo vim mc tree gnupg2 wget curl ca-certificates lsb-release debian-archive-keyring

# ==================== [ Main ] ====================

# --- [ Enable data forwarding between server's local interfaces ] ---

sudo echo "net.ipv4.ip_forward = 1" > /etc/sysctl.d/local.conf
sudo sysctl -p /etc/sysctl.d/local.conf


# --- [ Install and configure nginx ] ---

# Import nginx signing key
curl https://nginx.org/keys/nginx_signing.key | gpg --dearmor | sudo tee /usr/share/keyrings/nginx-archive-keyring.gpg >/dev/null

# Add nginx repository
sudo echo "deb [signed-by=/usr/share/keyrings/nginx-archive-keyring.gpg] http://nginx.org/packages/debian $(lsb_release -cs) nginx" > /etc/apt/sources.list.d/nginx.list

# Install nginx
sudo apt update && sudo apt install -y nginx

# Add nginx repository
sudo echo "deb [signed-by=/usr/share/keyrings/nginx-archive-keyring.gpg] http://nginx.org/packages/debian $(lsb_release -cs) nginx" > /etc/apt/sources.list.d/nginx.list

# Install nginx
sudo apt update && sudo apt install -y nginx

# Configure nginx load balancer
sudo mv $NGINX_DEFAULT_CONF $NGINX_DEFAULT_CONF.bak
sudo echo \
"
upstream backend {
    server $NGINX_UPSTREAM_SERVER1;
    server $NGINX_UPSTREAM_SERVER2;
    server $NGINX_UPSTREAM_SERVER3;
}

server {

    listen 80;

    location / {
        proxy_pass http://backend;
    }
}
"\
> /etc/nginx/conf.d/$NGINX_SERVER_NAME.conf

# Check nginx config, enable automatic start
sudo nginx -t && sudo systemctl enable nginx.service --now