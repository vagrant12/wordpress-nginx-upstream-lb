#!/bin/bash
# Prepare web server

# ==================== [ Variables ] ====================

NGINX_DEFAULT_CONF=/etc/nginx/conf.d/default.conf
NGINX_SITES_LOCATION=/usr/share/nginx
NGINX_ACCESS_LOG_DIR=/var/log/nginx
NGINX_SERVER_NAME=wordpress

PHP_FPM_VERSION=7.4
PHP_FPM_SOCKET_FILE=/var/run/php/php$PHP_FPM_VERSION-fpm-$NGINX_SERVER_NAME.sock

WP_DOWNLOAD_LINK=https://ru.wordpress.org/latest-ru_RU.tar.gz

DB_NAME=wordpress
DB_USER=wordpress
DB_PASSWORD=wordpress
DB_HOST=192.168.56.11

# ==================== [ General ] ====================

apt update && apt install -y sudo vim mc tree gnupg2 curl ca-certificates lsb-release debian-archive-keyring

# ==================== [ Main ] ====================

# --- [ Install and configure php-fpm] ---

# Install php and extensions
sudo apt update && sudo apt install -y php$PHP_FPM_VERSION-fpm php-mysql php-curl php-gd php-intl php-mbstring php-soap php-xml php-xmlrpc php-zip

# Create pool
sudo echo \
"[$NGINX_SERVER_NAME]
user = nginx
group = nginx
listen = $PHP_FPM_SOCKET_FILE
listen.owner = nginx
listen.group = nginx
php_admin_value[disable_functions] = exec,passthru,shell_exec,system
php_admin_flag[allow_url_fopen] = off
pm = dynamic 
pm.max_children = 75 
pm.start_servers = 10 
pm.min_spare_servers = 5 
pm.max_spare_servers = 20 
pm.process_idle_timeout = 10s
"\
> /etc/php/$PHP_FPM_VERSION/fpm/pool.d/$NGINX_SERVER_NAME.conf

# Start php-fpm and enable automatic start
sudo systemctl enable php$PHP_FPM_VERSION-fpm.service --now

# --- [ Install and configure nginx ] ---

# Import nginx signing key
curl https://nginx.org/keys/nginx_signing.key | gpg --dearmor | sudo tee /usr/share/keyrings/nginx-archive-keyring.gpg >/dev/null

# Add nginx repository
sudo echo "deb [signed-by=/usr/share/keyrings/nginx-archive-keyring.gpg] http://nginx.org/packages/debian $(lsb_release -cs) nginx" > /etc/apt/sources.list.d/nginx.list

# Install nginx
sudo apt update && sudo apt install -y nginx

# Configure nginx
sudo mkdir -p $NGINX_SITES_LOCATION/$NGINX_SERVER_NAME
sudo chmod -R 0755 $NGINX_SITES_LOCATION/$NGINX_SERVER_NAME
sudo chown nginx:nginx -R $NGINX_SITES_LOCATION/$NGINX_SERVER_NAME
sudo mv $NGINX_DEFAULT_CONF $NGINX_DEFAULT_CONF.bak
sudo echo \
"server {
    listen 80 default_server;
    server_name $NGINX_SERVER_NAME.local www.$NGINX_SERVER_NAME.local;

    location / {
        root   $NGINX_SITES_LOCATION/$NGINX_SERVER_NAME;
        index  index.html index.php;
        try_files \$uri \$uri/ /index.php\$is_args\$args;
    }

    access_log $NGINX_ACCESS_LOG_DIR/$NGINX_SERVER_NAME.access.log;
    error_log $NGINX_ACCESS_LOG_DIR/$NGINX_SERVER_NAME.error.log;

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

    location ~ \.php$ {
        root $NGINX_SERVER_NAME;
        fastcgi_pass unix:$PHP_FPM_SOCKET_FILE;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_index  index.php;
        fastcgi_param  SCRIPT_FILENAME  $NGINX_SITES_LOCATION/$NGINX_SERVER_NAME/\$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~ /\.ht {
        deny all;
    }
}
"\
> /etc/nginx/conf.d/$NGINX_SERVER_NAME.conf

# # --- [ Tests ] ---

# # Add index.html file
# sudo echo \
# "<html>
#     <body>
#         <h1>test</h1>
#     </body>
# </html>
# "\
# > $NGINX_SITES_LOCATION/$NGINX_SERVER_NAME/index.html

# # Add info.php file
# sudo echo \
# "<?php
# phpinfo();
# ?>
# "\
# > $NGINX_SITES_LOCATION/$NGINX_SERVER_NAME/info.php

# --- [ Install and configure wordpress ] ---

# Download and unpack wordpress archive
sudo wget --output-document=$NGINX_SITES_LOCATION/$NGINX_SERVER_NAME/wordpress.tar.gz $WP_DOWNLOAD_LINK
sudo tar -xzf $NGINX_SITES_LOCATION/$NGINX_SERVER_NAME/wordpress.tar.gz --strip-components=1 --directory=$NGINX_SITES_LOCATION/$NGINX_SERVER_NAME && \
sudo rm -f $NGINX_SITES_LOCATION/$NGINX_SERVER_NAME/wordpress.tar.gz
sudo chown nginx:nginx -R $NGINX_SITES_LOCATION/$NGINX_SERVER_NAME

# Configure wordpress
sudo mv $NGINX_SITES_LOCATION/$NGINX_SERVER_NAME/wp-config-sample.php $NGINX_SITES_LOCATION/$NGINX_SERVER_NAME/wp-config.php
sed -i "s/define( 'DB_NAME', 'database_name_here' );/define( 'DB_NAME', \'$DB_NAME\' );/g" $NGINX_SITES_LOCATION/$NGINX_SERVER_NAME/wp-config.php
sed -i "s/define( 'DB_USER', 'username_here' )/define( 'DB_USER', \'$DB_USER\' )/g" $NGINX_SITES_LOCATION/$NGINX_SERVER_NAME/wp-config.php
sed -i "s/define( 'DB_PASSWORD', 'password_here' );/define( 'DB_PASSWORD', \'$DB_PASSWORD\' );/g" $NGINX_SITES_LOCATION/$NGINX_SERVER_NAME/wp-config.php
sed -i "s/define( 'DB_HOST', 'localhost' );/define( 'DB_HOST', \'$DB_HOST\' );/g" $NGINX_SITES_LOCATION/$NGINX_SERVER_NAME/wp-config.php

# Check nginx config, enable automatic start and restart php-fpm
sudo systemctl restart php$PHP_FPM_VERSION-fpm.service
sudo nginx -t && sudo systemctl enable nginx.service --now